import numpy as np


from WassAge import *


def getYoungestDA(dfGeo):
    
    return getExtremalDA(dfGeo, LIST_AGE_YOUNG)


def getOldestDA(dfGeo):
    
    return getExtremalDA(dfGeo, LIST_AGE_OLD)



def getExtremalDA(dfGeo, listCols):
    
    """
    Return the index + the row (NOT the index! so use .iloc) corresponding to the DA with maximal np.sum(dfGeo[ listCols ] ,axis=1)
    
    """
    
    pop = np.sum(dfGeo[ listCols ] ,axis=1)
    
    DAIdx=np.argmax(pop.values)
    
    
    return DAIdx, dfGeo.iloc[DAIdx,: ]