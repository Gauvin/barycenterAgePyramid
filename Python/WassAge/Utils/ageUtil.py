import numpy as np
import re

from WassAge import *

def getAgeRepresentativeArray():
    
    """
    Get the first integer preceding "_" so 0_5 -> 5 and 100_more -> 100
    """
    
    listIntsAge = np.array([ int(re.match("\d+(?=_)", s).group(0)) for s in LIST_AGE_COLS ] )
    listIntsAge=listIntsAge.reshape(-1,1)
    
    return listIntsAge
