import geopandas as gpd
from pathlib import Path
import numpy as np

from WassAge import *




def readNeighShp(epsgProjInt=32198, city="Quebec"):
    
    """
    Read the vdp_quartiers shp file + reproject 
    """
    
    
    if np.isin(city, ["Quebec City","Quebec"]).any() :
        dfNeigh = gpd.read_file( Path(DATA_PATH) / "QuebecNeighbourhoods" / "vdq-quartier.shp" )
    else:
        dfNeigh = gpd.read_file( Path(DATA_PATH) / "MontrealNeighbourhoods" / "Quartiers_sociologiques_2014.shp" )
        dfNeigh["NOM"] = dfNeigh["Q_socio"] #make sur the neigh names are consistent - with some oop, we could implement a "getNeighID()" or something similar
        
    print(f"Reading neibourhoods from {city}")
    
    dfNeigh=dfNeigh.to_crs(epsg=epsgProjInt)
    
    return dfNeigh



def readDAShp(epsgProjInt=32198, city="Quebec"):
    
    """
    Read the age pyramid shp file (created with cancensus R package) + reproject 
    """
    
    #For backward compatibility, possible to use Quebec City
    if np.isin(city, ["Quebec City","Quebec"]).any()  :
        city="Quebec"
    
    #Read the shp
    dfGeo =  gpd.read_file( Path(DATA_PATH) / f"shp{city}DAAgePyramid" / f"shp{city}DAAgePyramid.shp" )
        
        
    #We have merged neigh for Qc and Mtl only
    #Make sur the neigh names are consistent - with some oop, we could implement a "getNeighID()" or something similar
    if city == "Montreal"   :
        dfGeo["NOM"] = dfGeo["Q_socio"] 
    
    print(f"Reading neibourhoods from {city}")
    
    dfGeo = dfGeo.to_crs(epsg=epsgProjInt)
    
    return dfGeo