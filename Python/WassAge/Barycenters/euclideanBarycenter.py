from WassAge import * 
import pandas as pd


def getEuclideanBarycenter (dfGeoNoNull):
    
    return pd.DataFrame( dfGeoNoNull[LIST_AGE_COLS].mean(axis=0)).T