

import ot
from scipy.spatial import distance_matrix
import numpy as np
from warnings import warn

from WassAge.Utils import *



def getWassBarycenter(dfGeoNoNullNorm, distMatrix, reg = 1e-1,approx=True,verbose=False, extraParams={}):
    
    """
    Compute the approximate wasserstein barycenter using the distance matrix and a regularization term
    
    """
    
    #Make sure the df is normalized
    if np.all( abs( dfGeoNoNullNorm.sum(axis=1) -1 ) > 10**-4 ):
        warn("Warning in getWassBarycenterCityWide, the df is not (row) normalized")
    
    numDistr=dfGeoNoNullNorm.T.shape[1]
    weights=[1/numDistr] * numDistr
    if approx:
        print("Computing approximate sol:")
        bary_wass = ot.bregman.barycenter(dfGeoNoNullNorm[LIST_AGE_COLS].T ,
                                          distMatrix,
                                          reg, 
                                          weights=weights,
                                          method="sinkhorn",
                                          numItermax = 300,
                                          verbose=verbose,
					  **extraParams)
    else:
        print("Computing optimal LP sol:")
        bary_wass = ot.lp.barycenter(dfGeoNoNullNorm[LIST_AGE_COLS].T.values ,
                                    distMatrix,
                                    verbose = verbose,
				    **extraParams)
        
    return bary_wass



def getWassBarycenterOldYoung(dfGeoNoNullNorm, distMatrix, approx=True, otParams={"reg" : 1e-1,"verbose" : False}):
    
    youngestDAIdx , _ = getYoungestDA(dfGeoNoNullNorm)
    oldestDAIdx , _ = getOldestDA(dfGeoNoNullNorm)
    
    return getWassBarycenter(dfGeoNoNullNorm.iloc[ [oldestDAIdx,youngestDAIdx], : ], 
                              distMatrix, 
                              approx=approx,
                              **otParams)
    


def getDistanceAgeCohorts():
    
    """
    Compute the distance between the representative age of each age bucket (mid-point except for the 100+ cohort)
    assumes standard euclidean distance
    
    Returns a scaled matrix: each element is in [0,1]
    """
    
    listIntsAge = getAgeRepresentativeArray()
    
    distMatrix = distance_matrix(listIntsAge,listIntsAge)
 


    #Square to that d(5,10) = 5**2 
    #l2 norm
    distMatrix=distMatrix**2

    #Normalize in the hope of avoiding numerical issues
    distMatrix=distMatrix/np.max(distMatrix)
    
    return distMatrix
