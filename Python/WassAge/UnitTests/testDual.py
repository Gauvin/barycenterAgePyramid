from WassAge.CustomOptimization import *


def testDual(A, ainit, M, printSol=False):


    dualSolDict, wassDist = wassDistanceFixedDistDual(A[:, 0], ainit, M)
    dualSol = list(dualSolDict.values())

    print(f"Wass distance is: {wassDist}")

    if printSol:
        print("Dual sol is:")
        print(dualSol)

    return dualSol, wassDist