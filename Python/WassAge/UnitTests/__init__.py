from WassAge.UnitTests.testDual import *
from WassAge.UnitTests.testPrimal import *
from WassAge.UnitTests.testDualPrimalSame import *

from WassAge.UnitTests.testProjectedGradient import *
from WassAge.UnitTests.testWassersteinSame import *