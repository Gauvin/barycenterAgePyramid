import ot
from pytictoc import TicToc
import numpy as np

from WassAge.UnitTests import *
from WassAge.DataEngineering import *
from WassAge.CustomOptimization import *
from WassAge.Barycenters import *

def testWassersteinSameSimulation(stepSize=None, verbose=False):

    #For time
    t= TicToc()


    n_distributions = np.random.choice([2, 4], 1)[0]
    n = np.random.randint(3, 15, 1)[0]

    M, A = createTestDistributions(n_distributions, n)
    ainit = createTestUniformDist(n)

    t.tic()
    aManual = testProjectedGradientSimulatedDistributions(ainit,
                                                          A,
                                                          M,
                                                          stepSize=stepSize,
                                                          verbose=verbose)
    wassDistToBarycenterManual = computeAvgWassDist(aManual,
                                                    A,
                                                    M,
                                                    stepSize=stepSize,
                                                    verbose=verbose)
    t.toc()
    timeToSolveManual = t.tocvalues()

    t.tic()
    aOt = ot.lp.barycenter(A, M)
    wassDistToBarycenterManualOT = computeAvgWassDist(aOt, A, M)
    t.toc()
    timeToSolveOT = t.tocvalue()


    print("Manual sol:")
    print(aManual)


    print("OT sol:")
    print(aOt)

    print(f"Manual dist : {wassDistToBarycenterManual} vs OT dist: {wassDistToBarycenterManualOT} ")
    print(f"Total time: {timeToSolveManual} vs OT: {timeToSolveOT}")

    assert np.isclose(wassDistToBarycenterManual ,wassDistToBarycenterManualOT)


    return True



def testWassersteinSameRealData(subsetRows=50,verbose=False):

    #For time
    t= TicToc()

    #Geo data + renormalize to make sure sum is really 1 + select a subset of the DAs
    dfGeoClean = normalizeCleanDfGeo(readDAShp(city="Quebec City"))
    subsetRows = min(subsetRows, dfGeoClean.shape[0])
    dfSubsetRenormalized = dfGeoClean[LIST_AGE_COLS].div(dfGeoClean[LIST_AGE_COLS].sum(axis=1), axis=0)
    dfSubsetRenormalized = dfSubsetRenormalized.iloc[:subsetRows]

    #Age cohorts
    distMatrix = getDistanceAgeCohorts()
    numAgeGroup=distMatrix.shape[0]

    #Manual algo
    t.tic()
    ainit = np.ones(numAgeGroup) / numAgeGroup
    aManual =  projectedGradientDescentWassBarycenter(ainit,
                             A=dfSubsetRenormalized.T.values,
                             M=distMatrix,
                             stepSize=None,
                             verbose = verbose)
    t.toc()
    wassDistToBarycenterManual = computeAvgWassDist(aManual, dfSubsetRenormalized.T.values, distMatrix)
    timeToSolveManual = t.tocvalue()

    #OT algo
    t.tic()
    aOt = ot.lp.barycenter(dfSubsetRenormalized.T.values, distMatrix)
    wassDistToBarycenterManualOT = computeAvgWassDist(aOt, dfSubsetRenormalized.T.values, distMatrix)
    t.toc()
    timeToSolveOT = t.tocvalue()

    print("Manual sol:")
    print(aManual)

    print("OT sol:")
    print(aOt)

    print(f"Manual dist : {wassDistToBarycenterManual} vs OT dist: {wassDistToBarycenterManualOT} ")
    print(f"Total time: {timeToSolveManual} vs OT: {timeToSolveOT}")

    assert np.isclose(wassDistToBarycenterManual, wassDistToBarycenterManualOT)

    return True

