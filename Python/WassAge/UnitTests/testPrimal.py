from WassAge.CustomOptimization import *


def testPrimal(A, ainit, M, printSol=False):

    n=A.shape[0]

    primalSolDict, wassDist = wassDistanceFixedDistPrimal(A[:, 0], ainit, M)
    primalSol = np.array(list(primalSolDict.values()))

    print(f"Wass distance is: {wassDist}")

    if printSol:
        print("Primal sol is:")
        print(primalSol.reshape(n,n))

    return primalSol, wassDist