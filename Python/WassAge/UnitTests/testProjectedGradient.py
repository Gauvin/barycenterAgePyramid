import numpy as np

from WassAge.UnitTests.testDual import *
from WassAge.UnitTests.testPrimal import *

from WassAge.CustomOptimization import *



def testProjectedGradientSimulatedDistributions(ainit, A,M, stepSize):



    aopt= projectedGradientDescentWassBarycenter(ainit,
                                           A=A,
                                           M=M,
                                           stepSize=stepSize )

    assert np.isclose(np.sum(aopt), 1)
    print(aopt)

    return aopt