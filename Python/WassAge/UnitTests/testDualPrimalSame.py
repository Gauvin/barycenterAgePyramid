
import numpy as np

from WassAge.UnitTests.testDual import *
from WassAge.UnitTests.testPrimal import *

from WassAge.CustomOptimization import *



def testDualPrimalSame (numIters=10):

    for k in range(numIters):
        n_distributions = np.random.choice([2, 4], 1)[0]
        n = np.random.randint(3, 15, 1)[0]

        M, A = createTestDistributions(n_distributions, n)
        ainit = createTestUniformDist(n)

        _, wassDistDual = testDual(A, ainit, M)
        _, wassDistPrimal = testPrimal(A, ainit, M)

        assert np.isclose( wassDistDual , wassDistPrimal )

    return True