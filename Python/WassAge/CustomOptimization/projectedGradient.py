import numpy as np
import cvxpy as cp


from WassAge.CustomOptimization.dual import *



def projectOntoSimplex(x):
    
    """
    Projection: point that minimizes distance to convex set (simpelx) using norm 2
    99.9% sure there exists a relatively complex analytic solution for the problem, but solving the problem is probably less bug-prone
    """
    n= len(x)
    u=cp.Variable(n)
    normLoss=cp.norm2( x - u  ) 
    
      
    constList=[]
    constList.append( cp.sum(u) == 1)
    constList.append(u >= 0)
        
    probl=cp.Problem( cp.Minimize( normLoss ),
                     constraints=constList)
    
    probl.solve()
    
    return u.value




def projectedGradientDescentWassBarycenter(ainit, A, M, stepSize=10**-3, maxIter=1000,verbose=False):
    
    numDist=A.shape[1]
    aold=ainit
    anew=ainit
    print(f"Considering {numDist} distributions")
    
    #Iterate until convergence or fixed number of iterations
    for iter in range(maxIter):
        
        #compute the gradient as the average of all dual opt solutions to the N wass distances
        grad=np.zeros_like(ainit)
        for i in range(numDist):
            gradDict, _ =wassDistanceFixedDistDual(aold,A[:,i],M)
            grad += np.array(list(gradDict.values()))/numDist

        if np.sum( grad**2 )**0.5 < 10**-5:
            print("breaking out, the gradient is too close to zero")
            print(grad)
            break

        #Use a decreasing step size => diverging sequence so should converge
        #Using a naive linesearch is probably not a good idea:
        #Function evaluation is costly: requires evaluating sum_k 1/N * W(anew, b_k) each time we want to determine the step size
        # which requires solving k linear programs
        if stepSize == None:
            stepSize = 1/(500+iter)

        anewTentative= aold- grad *stepSize
        anew=projectOntoSimplex(anewTentative)
        
        if np.sum( (anew-aold)**2 )**0.5 < 10**-5:
            print("breaking out, the iterates are too close to one another")
            break
        aold=anew

        if verbose:
            print(f"Iter:{iter}: ")
            print(anew.reshape(1,-1))

    return anew