def getCode(code, solver):
    if code == solver.OPTIMAL :
        return "OPTIMAL"
    elif code == solver.FEASIBLE:
        return "FEASIBLE"
    elif  code == solver.INFEASIBLE:
        return "INFEASIBLE"
    elif  code == solver.UNBOUNDED:
        return "UNBOUNDED"
    elif  code == solver.ABNORMAL:
        return "ABNORMAL"
    elif  code == solver.MODEL_INVALID:
        return "MODEL_INVALID"
    elif  code == solver. NOT_SOLVED:
        return "NOT_SOLVED"
    else:
        return "NA code"