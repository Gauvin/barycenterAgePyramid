
from ortools.linear_solver import pywraplp


def wassDistanceFixedDistPrimal(a,b,M):

    if a.shape != b.shape:
        raise exception ("Fatal error! a and b matrices do not have the same dimension!")
        
    if a.shape[0] != M.shape[0]:
         raise exception ("Fatal error! distributions do not have the same number of atoms as the ground distance matrix!")
    
    #1
    # Solver
    solver = pywraplp.Solver('WassDistPrimal', pywraplp.Solver.GLOP_LINEAR_PROGRAMMING)


    #2
    # Dual vars associated to  sum_i pî_ij = b_j and sum_j pî_ij = a_i
    dictVarPi={}

    #pi_ij : flow variables
    for i in range(M.shape[0]):
        for j in range(M.shape[1]):
            dictVarPi[i,j] =   solver.NumVar(0, solver.infinity(), f'pi_{i}{j}')  #>=0


    #3
    #Constraints sum_i pi_ij = M_j 
    dictConstraintsA={}
    for j in range(M.shape[0]):
        dictConstraintsA[j] = solver.Constraint(a[j], a[j])
        for i in range(M.shape[1]):
            dictConstraintsA[j].SetCoefficient( dictVarPi[i,j] ,1 )

    #Constraints sum_j pi_ij = M_j 
    dictConstraintsB={}    
    for i in range(M.shape[1]):
        dictConstraintsB[i] = solver.Constraint( b[i], b[i])
        for j in range(M.shape[0]):
            dictConstraintsB[i].SetCoefficient( dictVarPi[i,j] ,1 )
        
        
    #4
    #Objective function
    objective = solver.Objective()
    objective.SetMinimization()     #primal: min assignment
    for i in range(M.shape[0]):
        for j in range(M.shape[1]):
             objective.SetCoefficient(dictVarPi[i,j], M[ i , j])

    #Solve
    solveCode=solver.Solve()    
    strCode=getCode(solveCode,solver)
    
    if solveCode != 0:
        print(f"Fatal error, could not solve the primal! - {strCode}")
        print(a)
        print(b)
        return {i: 0 for i in range(M.shape[0])}, 0
    
    #Get the cost
    cost=0
    for i in range(M.shape[0]):
        for j in range(M.shape[1]):
            cost += dictVarPi[i,j].solution_value() * M[i,j] 
    
    dictVarPiSol={}
    dictVarPiSol = { (i,j):  dictVarPi[i,j].solution_value() for (i,j) in dictVarPi.keys() }

    
    return dictVarPiSol, cost