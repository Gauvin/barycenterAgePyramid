from WassAge.CustomOptimization.dual import * 
from WassAge.CustomOptimization.primal import * 


def computeAvgWassDist(a, A,M, useDual=True):
    
    numDist=A.shape[1]
    wassDist=0

    strMess = "dual" if useDual else "primal"
    print(f"Considering {strMess} when computing wass distance over {numDist} distributions")
   
    #Take average over the A.shape[1] distributions 
    for i in range(numDist):
        _, dist = wassDistanceFixedDistDual(a,A[:,i],M) if useDual else wassDistanceFixedDistPrimal(a,A[:,i],M)
        wassDist += dist/numDist
        
    return wassDist