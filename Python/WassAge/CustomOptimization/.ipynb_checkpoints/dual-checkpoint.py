from ortools.linear_solver import pywraplp


def wassDistanceFixedDistDual(a,b,M):
    
    #1
    # Solver
    solver = pywraplp.Solver('DualTransportion', pywraplp.Solver.GLOP_LINEAR_PROGRAMMING)


    #2
    # Dual vars associated to  sum_i pî_ij = b_j and sum_j pî_ij = a_i
    dictVarAlpha={}
    dictVarBeta={}

    #pi_ij : flow variables
    for i in range(M.shape[0]):
        dictVarBeta[i] =   solver.NumVar(-solver.infinity(), solver.infinity(), f'beta_{i}')  #urs continuous variable 

    for j in range(M.shape[1]):
        dictVarAlpha[j] =   solver.NumVar(-solver.infinity(), solver.infinity(), f'alpha_{j}')  #urs continuous variable 



    #3
    #Constraints alpha_i + beta_j \leq M_ij 
    dictConstraints={}
    for i in range(M.shape[1]):
        for j in range(M.shape[0]):
            dictConstraints[i,j] = solver.Constraint(-solver.infinity(), M[i,j])
            dictConstraints[i,j].SetCoefficient( dictVarAlpha[j] ,1 )
            dictConstraints[i,j].SetCoefficient( dictVarBeta[i] ,1 )

    #4
    #Objective function
    objective = solver.Objective() 
    objective.SetMaximization()                                    #dual is *maximization* problem 
    
    for i in range(M.shape[0]):
         objective.SetCoefficient(dictVarBeta[i], b[i])

    for j in range(M.shape[1]):
         objective.SetCoefficient(dictVarAlpha[j], a[j])
        
    #5 Solve
    solveCode=solver.Solve()    
    strCode=getCode(solveCode,solver)
    print(f"Solve code is:{solveCode}")
        
    if solveCode != 0:
        print(f"Fatal error, could not solve the dual! - returning 0 gradient - {strCode}")
        print(a)
        print(b)
        return {i: 0 for i in range(M.shape[1])}, 0
        
    #6 Get the solution 
    cost = 0 
    for i in range(M.shape[0]):
        cost += dictVarBeta[i].solution_value() * b[i] 
    for j in range(M.shape[1]):
        cost += dictVarAlpha[j].solution_value() * a[j] 
            
    return {i : dictVarAlpha[i].solution_value() for i in dictVarAlpha.keys()}, cost