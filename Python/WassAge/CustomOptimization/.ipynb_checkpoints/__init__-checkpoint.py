from WassAge.CustomOptimization.codes import * 
from WassAge.CustomOptimization.dual import * 
from WassAge.CustomOptimization.primal import * 
from WassAge.CustomOptimization.projectedGradient import * 
from WassAge.CustomOptimization.wassersteinUtil import * 
from WassAge.CustomOptimization.benchmarkTest import * 