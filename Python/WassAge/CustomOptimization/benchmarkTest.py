import numpy as np
import scipy as sp 



def createTestDistributions(n_distributions = 4,
                            n = 3):


    #Create the ground distance
    colVector=np.arange(n).reshape(-1,1)
    #make it square otherwise the triangle ineq is tight
    M=sp.spatial.distance_matrix(colVector, colVector)**2 
    


    #Now create the distributions

    A = np.random.randint(1, 10, (n_distributions, n))
    A = A/A.sum( axis=1)[:, np.newaxis]

    #Account for small numerical imprecisions
    if np.all( np.isclose(  A.sum(axis=1), 1 ) ) == False:
        raise Exception("Fatal error normalizing A matrix")

    A=np.asarray(A.T)

    return M, A




def createTestUniformDist(n = 3):

    ainit = np.ones(n) / n
    return ainit
