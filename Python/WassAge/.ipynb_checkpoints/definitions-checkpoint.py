DATA_PATH = "/home/charles/Projects/BarycentersAgePyramid/Data"
#DATA_PATH = "/home/dhw3172/projects/barycenterAgePyramid/Data"


LIST_AGE_COLS = [ "0_4",
                   "5_9" ,
                   "10_14",
                    "15_19" ,
                   "20_24"   ,
                   "25_29", 
                    "30_34"   ,
                   "35_39" ,
                   "40_44" ,
                    "45_49"  ,
                    "50_54" ,
                   "55_59"  ,
                   "60_64"   ,
                    "65_69",
                   "70_74" ,
                   "75_79"     ,
                   "80_84"    ,
                   "85_89"  ,
                    "90_94"    ,
                   "95_99"  ,
                   "100_+"
              ]

NUM_AGE_BINS=len(LIST_AGE_COLS)

LIST_AGE_OLD =['80_84', '85_89', '90_94', '95_99', '100_+']


LIST_AGE_YOUNG = ['0_4', '5_9', '10_14', '15_19', '20_24']