from WassAge.IO import *


import re
import pandas as pd
import numpy as np





def normalizeCleanDfGeo(dfGeo):
    
    """
    Read in the DA age pyramid shp file where rows are DAs/obs and cols are the age cohorts 
    + row normalize (so that each DA age pyramid corresponds to a distribution)
    """
    
    if np.all(np.isin(LIST_AGE_COLS, dfGeo.columns)) == False:
        raise Exception("Fatal error in normalizeCleanDfGeo, the age columns are not all present")
    
    #Remove Nas
    dfGeoNoNull = dfGeo.dropna()
        
    #Copy df
    dfGeoNoNullNorm = dfGeoNoNull.copy()

    #Normalize + Select age columns
    dfGeoNoNullNorm[ LIST_AGE_COLS ]=dfGeoNoNullNorm[ LIST_AGE_COLS ].div( dfGeoNoNullNorm[ LIST_AGE_COLS ].sum(axis=1), axis=0)
    
    return dfGeoNoNullNorm



def aggregateCleanDfGeo(dfGeo):
    
    """
    Sum over all DAs forming the city => this gives the "real" aggregated population pyramid
    """
    
    
    if np.all(np.isin(LIST_AGE_COLS, dfGeo.columns)) == False:
        raise Exception("Fatal error in normalizeCleanDfGeo, the age columns are not all present")
    
    #Remove Nas
    dfGeoNoNull = dfGeo.dropna()
 
    #Normalize + Select age columns
    dfGeoNoNullAgg = pd.DataFrame( dfGeoNoNull[ LIST_AGE_COLS ].sum(axis=0) )
    
    #Rename (unique) col
    dfGeoNoNullAgg.rename(columns={0:"Population"},inplace=True)
    
    #Normalize by dividing ove total pop so we get a distribution
    dfGeoNoNullAgg = dfGeoNoNullAgg/np.sum(dfGeoNoNullAgg["Population"])
    
    
    return dfGeoNoNullAgg
        
   